"""Unittest für controller.change_attribues()"""

import unittest
from classes import bitConverterClass


class TestGetSize(unittest.TestCase):
    """Unittests für bitConverterClass.Converter().get_size(bytes)"""

    def test_get_size(self):
        converter = bitConverterClass.Converter()
        kbytes = 2490
        mbytes = 30310770
        gbytes = 7486996119
        self.assertEqual('2.43KB', converter.get_size(kbytes))
        self.assertEqual('28.91MB', converter.get_size(mbytes))
        self.assertEqual('6.97GB', converter.get_size(gbytes))


if __name__ == '__main__':
    unittest.main()
