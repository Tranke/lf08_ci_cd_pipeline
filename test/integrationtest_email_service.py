"""Unittest für templater.get_template1()"""

import unittest
from classes import alarm
from classes import templater

class TestGetSystem(unittest.TestCase):
    """Unittests für bitConverterClass.Converter().get_size(bytes)"""
    def test_email_server_connection(self):
        Email_Service = alarm.SmtpServer()
        Templater = templater.Templater()

        self.assertEqual("{'T': (501, b'5.1.3 Invalid address')}", str(Email_Service.send_mail("Robert", "Test_SMTP_Python@outlook.de", Templater.get_template1("Robert","CPU",""))))
if __name__ == '__main__':
    unittest.main()