"""Unittest für templater.get_template1()"""

import unittest
from classes import templater


class TestGetSystem(unittest.TestCase):
    """Unittests für bitConverterClass.Converter().get_size(bytes)"""

    def test_get_system(self):
        Templater = templater.Templater()
        self.assertEqual('Hello Robert, the following Process: CPU has surpassed its threshold and is at97.5 %!!!!!Please check what is going on and perform error handling if necessary.Please do not reply to this email as it will be sent automatically by the system.Your Monitoring System',
                         Templater.get_template1("Robert","CPU","97.5"))
        print(Templater.get_template1("Robert","CPU","97.5"))
if __name__ == '__main__':
    unittest.main()
