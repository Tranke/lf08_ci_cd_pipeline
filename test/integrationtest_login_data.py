"""Unittest für userClass.py)"""

import unittest
from classes import userClass

class TestCheckLogin(unittest.TestCase):
	"""Unittests für login_check(username, password)"""
	def test_login(self):

		#Test erfolgreich
		username = "Admin"
		password = "123456"
		self.assertEqual("None", str(userClass.User.login_check(username, password)))

		#Test schlägt fehl
		username = "Admin"
		password = "TestFalsch"
		self.assertEqual("Password is wrong.", str(userClass.User.login_check(username, password)))

		if __name__ == '__main__':
			unittest.main()