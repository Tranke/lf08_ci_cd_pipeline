"""Unittest für systemClass.get_System()"""

import unittest
from classes import systemClass


class TestGetSystem(unittest.TestCase):
    """Unittests für bitConverterClass.Converter().get_size(bytes)"""

    def test_get_system(self):
        system_info = systemClass.SystemInfo()
        self.assertEqual('Linux', system_info.get_system())


if __name__ == '__main__':
    unittest.main()
