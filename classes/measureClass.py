import psutil
# from bitConverterClass import Converter
# from classes import bitConverterClass

from datetime import datetime
#Converter = Converter()
vmem = psutil.virtual_memory()


# Converter = Converter()
vmem = psutil.virtual_memory()


class Measurer(object):

    def __init__(self):
        print("")

    # CPU
    @staticmethod
    def measure_cpu_usage_per_cpu():
        for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
            return "Core {0}: {1} %".format(i + 1, percentage)

    @staticmethod
    def measure_cpu_usage_total():
        return psutil.cpu_percent()

    # memory
    @staticmethod
    def get_memory_total():
        return vmem.total

    @staticmethod
    def get_memory_used():
        return vmem.available

    @staticmethod
    def get_memory_avaiable():
        return vmem.used

    @staticmethod
    def get_memory_percent():
        return vmem.percent

    # partitions
    @staticmethod
    def get_partition_total():
        partitions = psutil.disk_partitions(all=False)
        for partition in partitions:
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                pass
            return partition_usage.total
            break

    @staticmethod
    def get_partition_usage():
        partitions = psutil.disk_partitions(all=False)
        for partition in partitions:
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                pass
            return partition_usage.used
            break

    @staticmethod
    def get_partition_free():
        partitions = psutil.disk_partitions(all=False)
        for partition in partitions:
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                pass
            return partition_usage.free
            break

    @staticmethod
    def get_partition_percent():
        partitions = psutil.disk_partitions(all=False)
        for partition in partitions:
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                pass
            return partition_usage.percent
            break


if __name__ == '__main__':
    Measurer = Measurer()
    print("=" * 40, "CPU Info", "=" * 40)
    Measurer.measure_cpu_usage_per_cpu()
    Measurer.measure_cpu_usage_total()
    print("=" * 40, "Memory Information", "=" * 40)
    Measurer.get_memory_total()
    Measurer.get_memory_used()
    Measurer.get_memory_avaiable()
    Measurer.get_memory_percent()
    print("=" * 40, "Disk Information", "=" * 40)
    Measurer.get_partition_total()
    Measurer.get_partition_usage()
    Measurer.get_partition_free()
    Measurer.get_partition_percent()