from string import Template
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from classes.measureClass import Measurer


class SmtpServer:
    @staticmethod
    def getContacts(filename):
        names = []
        emails = []
        with open(filename, mode='r', encoding='utf-8') as contactsFile:
            for a_contact in contactsFile:
                names.append(a_contact.split()[0])
                emails.append(a_contact.split()[1])
            return names, emails


    @staticmethod
    def send_mail(names, emails, message):

        MEINE_ADRESSE = "Test_SMTP_Python@outlook.de"
        PASSWORT = "TestMailServerPython123"
        senderror = None

        try:
            s = smtplib.SMTP(host='smtp.office365.com', port='587')
            s.starttls()
            s.login(MEINE_ADRESSE, PASSWORT)

            for name, email in zip(names, emails, ):
                msg = MIMEMultipart()

                # Parameters der Nachricht vorbereiten
                msg['From'] = MEINE_ADRESSE
                msg['To'] = email
                msg['Subject'] = "SICHERHEITSWARNUNG"

                # Nachricht als Mail hinzufügen
                msg.attach(MIMEText(message, 'plain'))

                # Nachricht über den eingerichteten SMTP-Server abschicken
                s.send_message(msg)

                del msg
            s.quit()
            return senderror
        except smtplib.SMTPException as senderror:

            return senderror




if __name__ == '__main__':
    print("")
