class Templater(object):
    def __init__(self):

        self.name = ""
        self.load = ""
        self.percent = ""

        self.template1 = f"Hello {self.name}, the following Process: {self.load} has surpassed its threshold and is at" \
                         f"{self.percent} %!!!!!" \
                         "Please check what is going on and perform error handling if necessary." \
                         "Please do not reply to this email as it will be sent automatically by the system." \
                         "Your Monitoring System"

    def get_template1(self, name, load, percent):
        self.name = name
        self.load = load
        self.percent = percent
        self.template1 = f"Hello {self.name}, the following Process: {self.load} has surpassed its threshold and is at" \
                         f"{self.percent} %!!!!!" \
                         "Please check what is going on and perform error handling if necessary." \
                         "Please do not reply to this email as it will be sent automatically by the system." \
                         "Your Monitoring System"
        return self.template1
