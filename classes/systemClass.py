import platform

class SystemInfo(object):

    def __init__(self):
        uname = platform.uname()
        self.systemName = uname.node
        self.system = uname.system
        self.release = uname.release
        self.version = uname.version
        self.machine = uname.machine
        self.processor = uname.processor

    def get_system_name(self):
        return self.systemName

    def get_system(self):
        return self.system

    def get_release(self):
        return self.release
    
    def get_version(self):
        return self.version

    def get_machine(self):
        return self.machine

    def get_processor(self):
        return self.processor
# print("="*40, "System Information", "="*40)
# uname = platform.uname()
# print(f"System Name: {uname.node}")
# print(f"System: {uname.system}")
# print(f"Release: {uname.release}")
# print(f"Version: {uname.version}")
# print(f"Machine: {uname.machine}")
# print(f"Processor: {uname.processor}")


if __name__ == '__main__':
    System = SystemInfo()
    print(System.get_info())