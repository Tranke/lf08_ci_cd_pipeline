class Converter(object):
    # Converter class to convert bits into a proper format

    def __init__(self):
        self.factor = 1024

    def get_size(self, bytes, suffix="B"):
    
        for unit in ["", "K", "M", "G", "T", "P"]:
            if bytes < self.factor:
                return f"{bytes:.2f}{unit}{suffix}"
            bytes /= self.factor