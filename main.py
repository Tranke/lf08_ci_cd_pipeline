from tkinter import Tk, Frame, Entry, messagebox, Toplevel, Label, Button
from classes import systemClass, templater
from classes import measureClass
from classes import bitConverterClass
from classes import userClass
from classes import templater
from classes import alarm
from datetime import datetime
import os
import logging

# Variables
now = datetime.now()
TSTAMP = now.strftime('%y' + '%m' + '%d')
TSTAMP2 = now.strftime('%Y-%m-%d %H:%M:%S')
'''Logging'''
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='logs/monitor_' + TSTAMP + '.log',
                    filemode='a')
logger = logging.getLogger(__name__)

logger.info("-------------------------------------------------\n")
logger.info('Datum: %s \n', TSTAMP2)


def main():
    root = Tk()
    app = login(root)
    root.mainloop()


class login:
    def __init__(self, master):
        self.master = master
        self.master.title("User Log In")
        self.master.geometry('300x200')
        self.frame = Frame(self.master)
        self.frame.pack(fill="both", expand=True)

        self.UsernameEntry = Entry(self.frame)
        self.UsernameEntry.grid(row=2, column=0)
        self.PasswordEntry = Entry(self.frame)
        self.PasswordEntry.grid(row=3, column=0)

        self.btnLogin = Button(self.frame, text='login', width=17, command=self.login_system)
        self.btnLogin.grid(row=4, column=0)

    def on_closing(self):  # Add this event handler
        self.master.destroy()

    def login_system(self):
        U = self.UsernameEntry.get()
        P = self.PasswordEntry.get()
        logging.info("logging in...")
        loginerror = userClass.User.login_check(U,P)

        if  loginerror is None:
            self.master.withdraw()  # Do NOT call destroy as you need the root to be
            # active for Toplevel
            self.newWindow = Toplevel(self.master)
            self.newWindow.protocol("WM_DELETE_WINDOW", self.on_closing)  # And add this

            self.app = systemScreen(self.newWindow)
            logging.info(f"logged in successfully.. {U}")
        else:
            self.UsernameEntry.delete(0, "end")
            self.PasswordEntry.delete(0, "end")
            messagebox.showerror("Login Error", loginerror)
            logging.info(f"login failed: {loginerror}")


class systemScreen:

    def __init__(self, master):
        System_View = systemClass.SystemInfo()
        master.geometry('500x200')
        master.title('System Overview')
        User_Label = Label(master, text="User:")
        User_Label.grid(row=3, column=2)
        User_Nick_Label = Label(master, text=f"Admin")
        User_Nick_Label.grid(row=3, column=4)

        System_Name_Label = Label(master, text="System name:")
        System_Name_Label.grid(row=14, column=5)
        System_Name_Value_Label = Label(master, text=System_View.get_system_name())
        System_Name_Value_Label.grid(row=14, column=7)

        System_Label = Label(master, text="System:")
        System_Label.grid(row=18, column=5)
        System_Value_Label = Label(master, text=System_View.get_system())
        System_Value_Label.grid(row=18, column=7)
        release_Label = Label(master, text="Release:")
        release_Label.grid(row=22, column=5)
        release_Value_Label = Label(master, text=System_View.get_release())
        release_Value_Label.grid(row=22, column=7)
        version_Label = Label(master, text="Version:")
        version_Label.grid(row=26, column=5)
        version_Value_Label = Label(master, text=System_View.get_version())
        version_Value_Label.grid(row=26, column=7)
        machine_Label = Label(master, text="Machine:")
        machine_Label.grid(row=30, column=5)
        machine_Value_Label = Label(master, text=System_View.get_machine())
        machine_Value_Label.grid(row=30, column=7)
        processor_Label = Label(master, text="Processor:")
        processor_Label.grid(row=34, column=5)
        processor_Label = Label(master, text=System_View.get_processor())
        processor_Label.grid(row=34, column=7)

        btn_Monitor = Button(master, text='open monitoring', width=17, command=self.open_monitor)
        btn_Monitor.grid(row=50, column=7)

    def open_monitor(self):
        self.newWindow = Toplevel()
        self.app = MonitorScreen(self.newWindow)
        


class MonitorScreen:

    def __init__(self, master):

        Measurer = measureClass.Measurer()
        Converter = bitConverterClass.Converter()
        Templater = templater.Templater()
        Alarm = alarm.SmtpServer()
        master.geometry('400x400')
        master.title('Monitoring')
        User_Label = Label(master, text="User:")
        User_Label.grid(row=3, column=2)
        User_Nick_Label = Label(master, text="Admin")
        User_Nick_Label.grid(row=3, column=3)
        
        # CPU
        Cpu_Title_Label = Label(master, text="CPU Information")
        Cpu_Title_Label.grid(row=5, column=2)
        CpuUsage_Label = Label(master, text="CPU usage:")
        CpuUsage_Label.grid(row=6, column=3)
        cpu_Usage = Measurer.measure_cpu_usage_total()
        logging.info(f"CPU Usage at {cpu_Usage}")
        Cpu_Usage_Value_Label = Label(master, text=f"{cpu_Usage}%")
        Cpu_Usage_Value_Label.grid(row=6, column=5)
        if cpu_Usage > 90:
            if cpu_Usage > 95:
                logging.critical("WARNING CPU usage over 95%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1, Templater.get_template1("Robert","CPU",cpu_Usage))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
            else:
                logging.warning("WARNING CPU usage over 90%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1,
                                    Templater.get_template1("Robert", "CPU", cpu_Usage))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
        # MEMORY
        Memory_Title_Label = Label(master, text="Memory Information")
        Memory_Title_Label.grid(row=7, column=2)
        Memory_Total_Label = Label(master, text="Memory total:")
        Memory_Total_Label.grid(row=8, column=3)
        Memory_Total_Value_Label = Label(master, text=Converter.get_size(Measurer.get_memory_total()))
        Memory_Total_Value_Label.grid(row=8,column=5)
        Memory_Used_Label = Label(master, text="Memory used:")
        Memory_Used_Label.grid(row=9, column=3)
        Memory_Used_Value_Label = Label(master, text=Converter.get_size(Measurer.get_memory_used()))
        Memory_Used_Value_Label.grid(row=9,column=5)
        Memory_Avaiable_Label = Label(master, text="Memory avaiable:")
        Memory_Avaiable_Label.grid(row=10, column=3)
        Memory_Avaiable_Value_Label = Label(master, text=Converter.get_size(Measurer.get_memory_avaiable()))
        Memory_Avaiable_Value_Label.grid(row=10, column=5)
        Memory_Percent_Label = Label(master, text="Memory percent:")
        Memory_Percent_Label.grid(row=11, column=3)
        memory_Percent = Measurer.get_memory_percent()
        logging.info(f"Memory Usage at {memory_Percent}")
        if memory_Percent > 90:
            if memory_Percent > 95:
                logging.critical("WARNING MEMORY usage over 95%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1, Templater.get_template1("Robert", "memory", memory_Percent))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
            else:
                logging.warning("WARNING MEMORY usage over 90%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1,
                                    Templater.get_template1("Robert", "memory", memory_Percent))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
        Memory_Percent_Value_Label = Label(master, text=f"{memory_Percent}%")
        Memory_Percent_Value_Label.grid(row=11, column=5)
        # disk/partition
        Disk_Title_Label = Label(master, text="Disk Information")
        Disk_Title_Label.grid(row=12, column=2)
        Disk_Total_Label = Label(master, text="Disk total:")
        Disk_Total_Label.grid(row=13, column=3)
        Disk_Total_Value_Label = Label(master, text=Converter.get_size(Measurer.get_partition_total()))
        Disk_Total_Value_Label.grid(row=13,column=5)
        Disk_Used_Label = Label(master, text="Disk used:")
        Disk_Used_Label.grid(row=14, column=3)
        Disk_Used_Value_Label = Label(master, text=Converter.get_size(Measurer.get_partition_usage()))
        Disk_Used_Value_Label.grid(row=14,column=5)
        Disk_Avaiable_Label = Label(master, text="Disk avaiable:")
        Disk_Avaiable_Label.grid(row=15, column=3)
        Disk_Avaiable_Value_Label = Label(master, text=Converter.get_size(Measurer.get_partition_free()))
        Disk_Avaiable_Value_Label.grid(row=15,column=5)
        Disk_Percent_Label = Label(master, text="Disk percent:")
        Disk_Percent_Label.grid(row=16, column=3)
        disk_Percent = Measurer.get_partition_percent()
        logging.info(f"Disk load at {disk_Percent}")
        if disk_Percent > 80:
            if disk_Percent > 90:
                logging.critical("WARNING DISK load over 95%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1, Templater.get_template1("Robert", "disk load", disk_Percent))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
            else:
                logging.warning("WARNING DISK load usage over 90%")
                try:
                    names1, emails1 = Alarm.getContacts('Textdata/Contacts.txt')
                    Alarm.send_mail(names1, emails1,
                                    Templater.get_template1("Robert", "disk load", disk_Percent))
                    logging.info("E-mail has been sent")
                except:
                    logging.info("E-mail could not be sent")
        Disk_Percent_Value_Label = Label(master, text=f"{disk_Percent}%")
        Disk_Percent_Value_Label.grid(row=16, column=5)

        btn_Open_Log = Button(master, text='open daily log', width=17, command="")
        btn_Open_Log.grid(row=17, column=3)

    def open_log(self):
        os.startfile("logs/monitor_" + TSTAMP + ".log")
            

   
if __name__ == "__main__":
    main()
